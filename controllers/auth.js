const { response } = require('express');
const bcryptjs = require('bcryptjs');

const User = require('../models/user');
const { googleVerify } = require('../helpers/google-verify');

const login = async (req, res = response) => {
  const { email, password } = req.body;

  try {
    // Check if the email exists.
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(400).json({
        msg: `Email/Password aren't correct - email`
      });
    }

    // Check if user is active.
    if (!user.status) {
      return res.status(400).json({
        msg: `Email/Password aren't correct - status: false`
      });
    }

    // Check if password is correct.
    const validPassword = bcryptjs.compareSync(password, user.password);

    if (!validPassword) {
      return res.status(400).json({
        msg: `Email/Password aren't correct - password`
      });
    }

    // Generar el JWT
    const token = await generateJWT(user.id);

    res.json({
      user,
      token
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ msg: 'Server error' });
  }
};

const googleSignIn = async (req, res = response) => {
  const { id_token } = req.body;

  try {
    const { email, image, name } = await googleVerify(id_token);

    let user = await User.findOne({ email });

    if (!user) {
      // Create user
      const data = {
        name,
        email,
        password: ':P',
        image,
        google: true
      };

      user = new User(data);
      await user.save();
    }

    if (!user.status) {
      return res.status(401).json({
        msg: 'User blocked'
      });
    }

    const token = await generateJWT(user.id);

    res.json({
      user,
      token
    });
  } catch (error) {
    res.status(400).json({
      msg: 'Google token invalid'
    });
  }
};

module.exports = {
  login,
  googleSignIn
};
