const { response, request } = require('express');
const bcryptjs = require('bcryptjs');

const User = require('../models/user');

const getAll = async (req = request, res = response) => {
  const { limit = 5, from = 0 } = req.query;
  const query = { status: true };

  const [ total, users ] = await Promise.all([
    User.countDocuments(query),
    User.find(query)
    .skip(+from)
    .limit(+limit)
  ]);

  res.json({
    total,
    users
  });
};

const create = async (req = request, res = response) => {
  const { name,
          email,
          password,
          role } = req.body;
  const user = new User({ name, email, password, role });

  // Encrypt password
  const salt = bcryptjs.genSaltSync();
  user.password = bcryptjs.hashSync(password, salt);

  // Save in DB
  await user.save();

  res.json(user);
};

const update = async (req = request, res = response) => {
  const { id } = req.params;

  // Sacamos del body de la request los campos password y google que no se quieren modificar.
  const { _id, password, google, email, ...rest } = req.body;

  // Validar contra la base de datos.
  if (password) {
    // Encrypt password
    const salt = bcryptjs.genSaltSync();
    rest.password = bcryptjs.hashSync(password, salt);
  }

  const user = await User.findByIdAndUpdate(id, rest);

  res.json(user);
};

const partialUpdate = (req = request, res = response) => {
  res.json({
    msg: 'Patch API - controller'
  });
};

const remove = async (req = request, res = response) => {
  const { id } = req.params;

  // Eliminación física del registro.
  // const user = await User.findOneAndDelete(id);

  // Borrado lógico del registro (modificando la propiedad status a false).
  const user = await User.findByIdAndUpdate(id, { status: false });

  res.json(user);
};

module.exports = {
  getAll,
  create,
  update,
  partialUpdate,
  remove
};
