const mongoose = require('mongoose');

const dbConnection = async () => {
  try {
    await mongoose.connect(process.env.MONGODB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    });

    console.log('DB online');
  } catch (error) {
    console.error(error);
    throw new Error('Error in the DB connection');
  }
};

module.exports = {
  dbConnection
};
