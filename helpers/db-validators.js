const Role = require('../models/role');
const User = require('../models/user');

const isValidRole = async (role = '') => {
  const roleExist = await Role.findOne({ role });

  if (!roleExist) {
    throw new Error(`The role ${role} does not exist`);
  }
};

const existEmail = async (email = '') => {
  // Verify email already exist
  const existEmail = await User.findOne({ email });

  if (existEmail) {
    throw new Error(`The email ${email} already exists`);
  }
};

const existUserById = async (id) => {
  const existUser = await User.findById(id);

  if (!existUser) {
    throw new Error(`The ID ${id} doesn't exist`);
  }
};

module.exports = {
  isValidRole,
  existEmail,
  existUserById
};
