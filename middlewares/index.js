const validateFields = require('./validate-fields');
const validateJWT = require('./validate-jwt');
const isAdmin = require('./validate-roles');
const hasRole = require('./validate-roles');

module.exports = {
  ...validateFields,
  ...validateJWT,
  ...isAdmin,
  ...hasRole
};
