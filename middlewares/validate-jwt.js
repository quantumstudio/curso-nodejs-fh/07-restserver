const { response, request } = require('express');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

const validateJWT = async (req = req, res = response, next) => {
  const token = req.header('x-token');

  if (!token) {
    return res.status(401).json({
      msg: 'No token in the request'
    });
  }

  try {
    const { uid } = jwt.verify(token, process.env.SECRET_KEY);
    const user = await User.findById(uid);

    if (!user) {
      return res.status(401).json({
        msg: 'Invalid token'
      });
    }

    if (!user.status) {
      return res.status(401).json({
        msg: 'Invalid token'
      });
    }

    req.user = user;

    next();
  } catch (error) {
    console.error(error);
    res.status(401).json({
      msg: 'Invalid token'
    });
  }

  console.log(token);
};

module.exports = {
  validateJWT
};
