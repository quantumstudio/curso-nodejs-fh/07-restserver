const { Schema, model } = require('mongoose');

const UserSchema = Schema({
  name: {
    type: String,
    required: [true, 'The name is required']
  },
  email: {
    type: String,
    required: [true, 'The email is required'],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'The password is required']
  },
  image: {
    type: String
  },
  role: {
    type: String,
    required: [true, 'The role is required'],
    default: 'USER_ROLE',
    enum: ['ADMIN_ROLE', 'USER_ROLE']
  },
  status: {
    type: Boolean,
    default: true
  },
  google: {
    type: Boolean,
    default: false
  }
});

UserSchema.methods.toJSON = function () {
  /** Sacamos el __v y el password. El resto de campos los agrupamos
   * en la variable user para enviar el objeto a guardar sin los
   * campos __v y password. **/
  const { __v, password, _id, ...user } = this.toObject();
  user.uid = _id;

  return user;
};

module.exports = model('User', UserSchema);
