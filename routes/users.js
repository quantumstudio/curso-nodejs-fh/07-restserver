const { Router } = require('express');
const { check } = require('express-validator');

const {
  validateFields,
  validateJWT,
  isAdmin,
  hasRole } = require('../middlewares');

const {
  isValidRole,
  existEmail,
  existUserById
} = require('../helpers/db-validators');

const {
  getAll,
  create,
  update,
  partialUpdate,
  remove
} = require('../controllers/users');


const router = Router();

router.get('/', getAll);

router.post('/', [
  check('email', 'The email is invalid').isEmail(),
  check('email').custom(existEmail),
  check('name', 'The name is required').not().isEmpty(),
  check('password', 'The password must be longer than 6 characters').isLength({ min: 6 }),
  // check('role', 'Role invalid').isIn(['ADMIN_ROLE', 'USER_ROLE']),
  check('role').custom(isValidRole),
  validateFields
], create);

router.put('/:id', [
  check('id', 'ID invalid').isMongoId(),
  check('id').custom(existUserById),
  check('role').custom(isValidRole),
  validateFields
], update);

router.patch('/', partialUpdate);

router.delete('/:id', [
  validateJWT,
  // isAdmin,
  hasRole('ADMIN_ROLE', 'USER_ROLE'),
  check('id', 'ID invalid').isMongoId(),
  check('id').custom(existUserById),
  validateFields
], remove);

module.exports = router;
